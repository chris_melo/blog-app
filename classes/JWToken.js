const jwt = require("jsonwebtoken");

const JWT = {
    secret: process.env.JWT_SECRET || 'my_secret',
    maxAge: 60 * 60 //in seconds; equal to 1 hour
};

class JWToken {
    constructor({ username, token }) {
        this.username = username;
        this.token = token;

        this.secret = JWT.secret;
        this.maxAge = JWT.maxAge;
    }

    createToken() { //create json web token
          return jwt.sign(
            { //data
                 username: this.username
            },
            this.secret,
            {
                expiresIn: this.maxAge //in seconds
            }

          );
    }

    verifyToken() { //verify token from user cookie
        let result;
 
        jwt.verify(this.token, this.secret, (err, decodedToken) => {
          if(err) { //invalid token
             console.error(err);
          } else { //valid token
             result = decodedToken; //result gets decoded value
          }
       })
       return result; 
     }
 }
 



module.exports = JWToken;