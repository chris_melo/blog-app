class Author {
    constructor({ author_id, first_name, middle_name, last_name, email, gender, age, insert_dt }) {
        this.author_id = author_id;
        this.first_name = first_name;
        this.middle_name = middle_name;
        this.last_name = last_name;
        this.email = email;
        this.gender = gender;
        this.age = age;
        this.insert_dt = insert_dt; 
}

async insert(pool) {
    let author_id;

    const sql = "INSERT INTO BLOG.AUTHORS( AUTHOR_ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME, EMAIL, GENDER, AGE, INSERT_DT ) VALUES ( DEFAULT, $1, $2, $3, $4, $5, $6, NOW() ) RETURNING AUTHOR_ID";
    
    await pool.query( sql, [ this.first_name, this.middle_name, this.last_name, this.email, this.gender, this.age ] )
    .then( (result) => {
        if(result.rows.length > 0 ) {
            author_id = result.rows[0].author_id;
        }
    }).catch( (err) => {
        console.error(err);
    })
    ;
    return author_id;
}
   


update() {

}

 get() {
   
}




delete() {

}

}
module.exports = Author;
