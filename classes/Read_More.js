class Read_More {
    constructor({
       title, subtitle, content, image, author_id, insert_dt
    }) {
       
        this.title = title;
        this.subtitle = subtitle;
        this.content = content;
        this.image = image;
        this.author_id = author_id;
        this.insert_dt = insert_dt;
      
    }
   

    async get(pool, posts_id) {
        try {
            const sql = 'SELECT * FROM BLOG.POSTS WHERE POSTS_ID = $1';
            const result = await pool.query(sql, [posts_id]);
            return result.rows[0];
        } catch (err) {
            console.error('Error executing query:', err);
            throw err; // Ensure the error is propagated
        }
    }
    
    async update(pool, posts_id, Data) {
        try {
            const sql = 'UPDATE BLOG.POSTS SET title = $1, subtitle = $2, content = $3 WHERE POSTS_ID = $4';
            const values = [Data.title, Data.subtitle, Data.content, posts_id];
            await pool.query(sql, values);
        } catch (err) {
            console.error('Error updating data:', err);
            throw err;
        }
    }
}    
module.exports = Read_More;
