class User {
    constructor({ user_id, username, password, last_login_dt, author_id }) {
        this.user_id = user_id;
        this.username = username;
        this.password = password;
        this.last_login_dt = last_login_dt;
        this.author_id = author_id; 
}

async insert(pool) {
    let successfulInsert = false;

    const sql = "INSERT INTO BLOG.USERS( USER_ID, USERNAME, PASSWORD, LAST_LOGIN_DT, AUTHOR_ID ) VALUES ( DEFAULT, $1, $2, NULL, $3 ) RETURNING USER_ID";
    await pool.query( sql, [ this.username, this.password, this.author_id ] )
    .then( (result) => {
        if(result.rows.length > 0 ) {
            successfulInsert = true;
            }
    }).catch( (err) => {
        console.error(err);
    })
    ;
    return successfulInsert;
}


async update(pool) {
    let successfulUpdate = false;

    const sql = "UPDATE BLOG.USERS SET LAST_LOGIN_DT = NOW() WHERE USERNAME = $1 RETURNING USERNAME";
    await pool.query( sql, [ this.username] )
    .then( (result) => {
        if(result.rows.length > 0 ) {
            successfulUpdate = true;
            }
    }).catch( (err) => {
        console.error(err);
    })
    ;
    return successfulUpdate;
}



async get(pool) {
    let hashed_password;

    const sql = "SELECT PASSWORD FROM BLOG.USERS WHERE USERNAME = $1";
    await pool.query( sql, [ this.username ] )
    .then( (result) => {
        if(result.rows.length > 0 ) {
            hashed_password = result.rows[0].password;
            }
    }).catch( (err) => {
        console.error(err);
    })
    ;
    return hashed_password;
}



delete() {

}

}
module.exports = User;
