
class Add_Blog {

    constructor({ posts_id, title, subtitle, content, image, insert_dt, author_id, filename}) {
        this.posts_id = posts_id;
        this.title = title;
        this.subtitle = subtitle;
        this.content = content;
        this.image = image;
        this.insert_dt = insert_dt; 
        this.author_id = author_id; 
        this.filename = filename; 
       
     
    }

        async insert(pool) {
            let posts_id;

            const base64Filename = Buffer.from(this.image).toString('base64');
       
            const base64_image = `data:image/png;base64,${base64Filename}`;
        
            const sql = "INSERT INTO BLOG.POSTS(POSTS_ID, TITLE, SUBTITLE, CONTENT, IMAGE, INSERT_DT, AUTHOR_ID, FILENAME) VALUES (DEFAULT, $1, $2, $3,  $4, NOW(), $5, $6) RETURNING POSTS_ID";
            
       
            await pool.query( sql, [ this.title, this.subtitle, this.content, base64_image, this.author_id, this.filename ] )
           
            .then( (result) => {
                if(result.rows.length > 0 ) {
                    posts_id = result.rows[0].posts_id;
                }
            }).catch( (err) => {
                console.error(err);
            })
            ;
            return posts_id;
        }

    

update() {
   

}



delete() {

}

}
module.exports = Add_Blog;
