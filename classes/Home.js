class Home {
    constructor({
       title, subtitle, content, image, insert_dt
    }) {
       
        this.title = title;
        this.subtitle = subtitle;
        this.content = content;
        this.image = image;
        this.insert_dt = insert_dt;
      
    }
   

    async get(pool) {
       
        const sql = 'SELECT * FROM BLOG.POSTS';
        const result = await pool.query(sql);
     
        return result.rows;
    } catch (err) {
        console.error('Error executing query:', err);
       
    }
    
}
    
module.exports = Home;
