
class Update {

    constructor({ posts_id, title, subtitle, content, insert_dt, author_id}) {
        this.posts_id = posts_id;
        this.title = title;
        this.subtitle = subtitle;
      
        this.content = content;
        this.insert_dt = insert_dt; 
        this.author_id = author_id; 
       
     
    }

    async get(pool) {
        const result = await pool.query("SELECT * FROM BLOG.POSTS");
        return result.rows || [];
    } catch (err) {
        console.error('Error executing query:', err);
       
    }


        async update(pool) {
            let posts_id;
        
            const sql = "UPDATE BLOG.POSTS SET TITLE =$1, SUBTITLE = $2, CONTENT = $3, INSERT_DT = $4 WHERE POSTS_ID = $5";  
           
           
            await pool.query( sql, [ this.title, this.subtitle, this.content, this.author_id ] )
           
            .then( (result) => {
                if(result.rows.length > 0 ) {
                    posts_id = result.rows[0].posts_id;
                }
            }).catch( (err) => {
                console.error(err);
            })
            ;
            return posts_id;
        }

    
    


delete() {

}

}
module.exports = Update;
