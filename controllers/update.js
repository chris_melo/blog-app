const Update = require("../classes/Update");
 

function render(req,res, data) {
    res.render("pages/update", { 
        data,
        title: "Update Page"     
        
    });
}


   
        
   
async function get(req, res, pool) {
    try {
        const data = await fetchData(); // Replace with your data fetching logic
        if (data.length === 0) {
            return res.render("pages/update", { message: 'No data found.' });
        }
        res.render("pages/update", { data, title: "Update Page" });
    } catch (error) {
        console.error(error);
        // Handle the error appropriately
        res.status(500).send("Internal Server Error");
    }

}

  
 async function update(req, res, pool) {
 
    const { title, subtitle, content, author_id} = req.body;
   
    
   if( !title || !subtitle || !content) {  
  
   
    res.render("pages/update", { message: "Title, Subtitle and content are required." });

       } else {
    
  
             //insert into POSTS table
             const update = new Update({ title, subtitle, content, author_id});
             const posts_id = await update.insert(pool);
  
             if( !posts_id ) { 
              
                 render(res, "Unable to updates.", "");
             } else { //successful insert
            
                     render(res, "", "You have updated a new blog");
                    
                  }      
  
                 }
             }
             
            
module.exports = { update_get: get, update_update: update};

