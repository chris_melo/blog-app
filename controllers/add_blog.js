const Add_Blog = require('../classes/Add_Blog');
const Author = require("../classes/Author");
const fs = require('fs');
const multer = require('multer');
const crypto = require('crypto');
const path = require('path');

function render(req, res, error_msg, success_msg) {
    res.render("pages/add_blog", {
        title: "Add a Blog",
        isAuthenticated: req.isAuthenticated,
        error_msg,
        success_msg
    });
}

function get(req, res) {
    render(req, res, "", "");
}

// Storage
const storage = multer.diskStorage({
    destination: function (req, file, callback) {
        var dir = "./uploads";

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
        callback(null, dir);
    },

    // For Filename display as filename, timestamp & ext IN UPLOADS FOLDER
    filename: function (req, file, callback) {
        const originalname = file.originalname;
        const timestamp = Date.now();
        const fileExtension = path.extname(originalname);

        const newFilename = `${originalname.split('.')[0]}${fileExtension}`;

        callback(null, newFilename);
    }
});

// Check filesize and filetype
const upload = multer({
    storage: storage,
    limits: { fileSize: 10 * 1024 * 1024 },
    fileFilter: function (req, file, callback) {
        var allowedMimes = ['image/jpeg', 'image/png', 'image/jpg'];
        if (allowedMimes.includes(file.mimetype)) {
            callback(null, true);
        } else {
            callback(new Error('Invalid file type. Only JPEG and PNG'));
        }
    }
}).array('files', 1);

// Post
async function post(req, res, pool) {
    upload(req, res, async function (err) {
        if (err) {
            if (err.message === 'Invalid file type. Only JPEG and PNG') {
                render(req, res, 'Invalid file type. Only JPEG and PNG', '');
            } else {
                render(req, res, 'File size limit exceeded.', '');
            }
        } else {
            const title = req.body.title;
            const subtitle = req.body.subtitle;
            const content = req.body.content;

            // Validate title, subtitle, and content
            if (!title || !subtitle || !content) {
                render(req, res, 'Title, subtitle, and content are required.', '');
            } else {
                if (req.files && req.files.length > 0) {
                    const file = req.files[0];
                    const originalname = file.originalname;
                    const fileExtension = path.extname(originalname);

                    const timestamp = parseInt(originalname.split('-')[1]);

                    const newFilename = `${originalname.split('.')[0]}${fileExtension}`;
                    const filePath = `./uploads/${file.filename}`;
                    const data = fs.readFileSync(filePath);
                    const author_id = req.body.author_id;

                    const add_blog = new Add_Blog({
                        id: null,
                        image: file.originalname,
                        data: data,
                        filename: newFilename,
                        title: title,
                        subtitle: subtitle,
                        content: content,
                        author_id: author_id
                    });

                    const posts_id = await add_blog.insert(pool);

                    if (!posts_id) {
                        render(req, res, "Unable to add a new blog.", "");
                    } else {
                        render(req, res, "", "You have added a new blog");
                    }
                } else {
                    render(req, res, "No files uploaded.", "");
                }
            }
        }
    });
}

module.exports = { add_blog_get: get, add_blog_post: post };
