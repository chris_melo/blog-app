const bcryptjs = require("bcryptjs");
const Author = require("../classes/Author");
const User = require("../classes/User");




function render(req, res, error_msg, success_msg) {
res.render("pages/register", {
     title: "Register",
      error_msg,
      success_msg,
      isAuthenticated: req.isAuthenticated,
}); 

}

function get(req, res) {
  render(req, res, "", "");
}

function post(req, res, pool) {
    const {  username, password, first_name, middle_name, last_name, email, gender, age} = req.body;
    
   if( !username || !password || !first_name || !middle_name || !last_name ) {  
       render(req, res, "Username, Password, First Name, and last name are required.", "");
   } else {
  bcryptjs.genSalt( 10, (err, salt) => {
    bcryptjs.hash( password, salt, async (err, hash) => {
        if(err) {
            console.error(err);
            render(req, res, "Unable to encrypt password.", "");
        } else {
           //insert into AUTHORS table
           const author = new Author({ first_name, middle_name, last_name, email, gender, age });
           const author_id = await author.insert(pool);

           if( !author_id ) { //unsuccessful insert
               render(req, res, "Unable to insert data into authors table.", "");
           } else { //successful insert
                  //insert into USERS table
                  const user = new User({ username: username.toLowerCase(), password:hash, author_id });
                  const successfulInsert = await user.insert(pool);

                  if( !successfulInsert ) {
                      render(req, res, "Unable to insert data into USERS table.", "");
                  } else {
                    
                   render(req, res, "", "You have successfully created your account.");
                   
               }
           }
        }    
       
   } );
} );

}


}
  
module.exports = { register_get: get, register_post: post } 