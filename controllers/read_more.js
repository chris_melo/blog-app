const Read_More = require("../classes/Read_More");


function render(req,res) {
    res.render("pages/read_more", { 
        
        title: "Read More"     
        
    });
}


   
        
   
function get(req, res) {
    render(req, res, {});
}

async function get(pool, posts_id) {
  const dataInstance = new Read_More({});
  try {
      const Data = await dataInstance.get(pool, posts_id);
      return Data || {};
  } catch (error) {
      console.error('Error fetching data:', error);
      throw error; 
  }
}
module.exports = { read_more_get: get };

