const bcryptjs = require("bcryptjs");
const User = require("../classes/User");
const JWToken = require("../classes/JWToken");

function render(req, res, error_msg) {
    res.render("pages/login", {
         title: "Login",
          error_msg,
          isAuthenticated: req.isAuthenticated 
         }); 
    
    }
   
    function get(req, res) {
        render(req, res, "");
      }

      async function post(req, res, pool) {
        const { username, password } = req.body;       
        if ( !username || !password )  return render(req, res, "Username and password are required!"); 
           
        const user = new User({ username: username.toLowerCase(), password });
        const hashed_password = await user.get(pool);

        if( !hashed_password ) return render(req, res, "Username and Password does not exist!"); 
        
        await bcryptjs.compare( password, hashed_password )
          .then( async (isMatch) => {
              if( !isMatch ) return render(req, res, "Your Password is incorrect!");


             //update last login_dt
             const successfulUpdate = await user.update(pool);
             if ( !successfulUpdate ) return render(req, res, "Something went wrong!");
             
             
             //create jsonweb token
             const jwtoken = new JWToken({ username: user.username });
             const token = jwtoken.createToken();
            

             //create cookie containing the token
             const maxAge = jwtoken.maxAge * 1000; //maxAge (in milliseconds)
             res.cookie( 'jwt_auth', token, { httpOnly: false, maxAge, secure: true } );  //in milliseconds
             res.cookie( 'theme', 'dark_mode', { httpOnly:false, maxAge, secure: true } );
             
             //redirect to landing
              res.redirect('/home'); 
          } ).catch( (err) => {
            console.error(err);
            render(req, res, "Something went wrong!");
          })
          ;
    }
    

      module.exports = { login_get: get, login_post: post} 