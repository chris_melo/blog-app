const Home = require("../classes/Home");

function render(req, res, data) {
    res.render("pages/home", {
        data,
        posts_id,
        title: "Home",
        isAuthenticated: req.isAuthenticated,
    });
}

//function get(req, res) {
  //  render(req, res, "", "");
//}

function get(req, res, pool) {
    const homeData = new Home({});

    homeData.get(pool)
        .then(data => {
            const formattedData = data.map(item => ({
                ...item,
                imageData: `data:image/png;base64,${item.image}`, //  item.image base64-encoded image data
                filename: `/uploads/${item.filename}`, 
            }));

            res.render("pages/home", {
                data: formattedData || [],
                isAuthenticated: req.isAuthenticated,
                title: 'Home',
            });
        })
        .catch(error => {
            console.error(error);
            res.render("pages/home", {
                data: [],
                title: 'Home',
                message: 'Error fetching data.',
            });
        });
}

module.exports = { home_get: get };
