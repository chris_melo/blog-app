
let currentPage = 1;

function showPage(event, pageNumber) {
  event.preventDefault();
  const pages = document.querySelectorAll(".page");
  pages.forEach((page) => (page.style.display = "none"));
  document.getElementById(`page${pageNumber}`).style.display = "block";
  currentPage = pageNumber;
}
