const express = require("express");

const cookieParser = require("cookie-parser");

const app = express();
//const path = require('path');
 
app.set("view engine", "ejs");

//middlewares
app.use(express.urlencoded({ extended: true }) );
app.use(express.static("./public"));
app.use( cookieParser() );


app.use('/uploads', express.static('uploads'));
//app.use('/uploads', express.static(path.join(__dirname, 'uploads')));



const appRoutes = require("./routes/routes");
appRoutes(app);


//multer
app.use(express.json());

const PORT = process.env.PORT || 3000;

app.listen(PORT, function(err) {
    if(!err) console.log(`Started on port ${PORT}`);
     else console.error(err);
});
