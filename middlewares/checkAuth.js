const JWToken = require("../classes/JWToken");

module.exports = (req, res, next) => {
    req.isAuthenticated = false;
    
    
    let token = req.cookies.jwt_auth; //get token from cookie, available
    if (token) {   //available
      const jwToken = new JWToken ({token}); //create instance of JWtoken
    
      const result = jwToken.verifyToken(); //verify and get decoded token payload
      
      if(result) { //successfully verified
        req.username = result.username;
        req.isAuthenticated = true;

      }

    }
    next();
};

