const { Pool } = require("pg");
const pool = new Pool({ connectionString: "postgresql://postgres:chrisna@localhost:5432/blog-app" });

const checkAuth = require("../middlewares/checkAuth");
const requireAuth = require("../middlewares/requireAuth"); // pages that needs to be logged in
const forwardAuth = require("../middlewares/forwardAuth");

//isAuthenticated (for dynamic menu navigation)
module.exports = function(app) {
    app.use(checkAuth);
    
    //login
    const { login_get, login_post} = require("../controllers/login"); 
    app.get("/", forwardAuth, (req, res) => {  
	   login_get(req, res);
	});     

    app.post("/", (req, res) => {
        login_post(req, res, pool); 
      }); 

       //logout
     app.get("/logout", (req, res) => {
        res.cookie("jwt_auth", "", { maxAge: 1}); //1 millisecond
        res.cookie("theme", "", { maxAge: 1});

        res.redirect("/");
  });

    
    //register
    const { register_get, register_post }  = require("../controllers/register"); 
    app.get("/register", forwardAuth, (req, res) => {
    register_get(req, res); 
       });

     app.post("/register", (req, res) => {
       register_post(req, res, pool); 
     });                           
 
    
   const { home_get } = require("../controllers/home");

   app.get("/home", requireAuth, async (req, res) => {
       const data = await home_get(req, res, pool);
   
   });
   
 
    //add blog
    const { add_blog_get, add_blog_post}  = require("../controllers/add_blog"); 
    app.get("/add_blog", requireAuth, (req, res) => {
    add_blog_get(req, res); 
       });

   app.post("/add_blog", (req, res) => {
     add_blog_post(req, res, pool); 
    });   

    
    //GET AUTHOR
    //add blog
    const { author_get, author_post}  = require("../controllers/register"); 
    app.get("/add_blog", (req, res) => {
    author_get(req, res); 
       });

}